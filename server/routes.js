import examplesRouter from './api/controllers/examples/router'
import adidasRouters from './api/controllers/adidas/router'

export default function routes(app) {
  app.use('/api/v1/examples',examplesRouter);
  app.use('/api/v1/adidas',adidasRouters);
};