import AdidasService from '../../services/adidas.service';

export class Controller {

  allAdidasService(req, res) {
    AdidasService
    .allAdidasService()
    .then(r => res.json(r));
  }

  createAdidasService(req, res) {
    AdidasService
      .createAdidasService(req.body.name, req.body.price, req.body.color)
//      .createAdidasService(req.body.price)
 //     .createAdidasService(req.body.color)
      .then(r => res
        .status(201)
        .location(`/api/v1/adidas/${r.id}`)
        .json(r))
        .catch((error) => {
          console.log(error);
          return Promise.reject(new Error(error))
        });   

  } 
}

export default new Controller();