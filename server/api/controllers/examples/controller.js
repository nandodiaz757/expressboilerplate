import ExamplesService from '../../services/examples.service';

const { check, validationResult } = require('express-validator');

export class Controller {
  all(req, res) {
    ExamplesService
    .all()
    .then(r => res.json(r))
    .catch((error) => {
      console.log(error);
      return Promise.reject(new Error(error));
  })
  }

  create(req, res) {
    ExamplesService
      .create(req.body.name)
      .then(r => res
        .status(201)
        .location(`/api/v1/examples/${r.id}`)
        .json(r))
        .catch((error) => {
          console.log(error);
          return Promise.reject(new Error(error))
        });    
  } 
}

export default new Controller();