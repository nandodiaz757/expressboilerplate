import l from '../../common/logger';

let id = 0;


const adidas = [
  { id: id++, name: ' BUSENITZ VULC II', price: ' 144.900', color:' BLACK'},
  { id: id++, name: ' TENIS ALPHAEDGE 4D', price: ' 199.300', color:' RED AND BALACK' },
  { id: id++, name: ' TENIS DE BÁSQUET DAME 6', price: ' 174.100', color:' WHITE' }, 
];

export class AdidasService {
  allAdidasService() {
    l.info(`${this.constructor.name}.allAdidasService()`);
    return Promise.resolve(adidas);
  }

  createAdidasService(name, price, color) {
    const tenis = {
      id: id++,
      name,
      price,
      color
    };
    adidas.push(tenis);
    l.info(tenis, `${this.constructor.name}.create(${name})`, `${this.constructor.price}.create(${price})`, `${this.constructor.color}.create(${color})`);
    return Promise.resolve(tenis);
  }
}

export default new AdidasService();


